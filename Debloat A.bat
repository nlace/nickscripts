@echo off
echo Download latest guid blacklist from TRON project
wget --no-check-certificate  "https://raw.githubusercontent.com/bmrf/tron/master/resources/stage_2_de-bloat/oem/programs_to_target_by_GUID.txt" -O PGUID.txt

echo Now iterate over list and remove any entries present.
REM This is required so we can check the errorlevel inside the FOR loop
SETLOCAL ENABLEDELAYEDEXPANSION


REM Loop through the file...
for /f %%i in (PGUID.txt) do (
	REM  ...and for each line: a. check if it is a comment or SET command and b. perform the removal if not
	if not %%i==:: (
	if not %%i==set (

		echo %%i
		start /wait msiexec /qn /norestart /x %%i >> "log.txt" 2>nul

		REM Reset UpdateExeVolatile. I guess we could check to see if it's flipped, but eh, not really any point since we're just going to reset it anyway
		reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Updates" /v UpdateExeVolatile /d 0 /f >nul 2>&1

		REM Check if the uninstaller added entries to PendingFileRenameOperations. If it did, export the contents, nuke the key value, then continue on
		reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations >nul 2>&1
		if !errorlevel!==0 (
			echo found something 
			reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations
			reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations>> "PendingFileRenameOperations_%COMPUTERNAME%_%CUR_DATE%.txt"
			reg delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations /f >nul 2>&1
			)

		REM Running tick counter to a separate raw log file so we can see if the script stalls on a particular GUID.
		REM Not displayed to console or dumped to main log to avoid cluttering them up
		set /a TICKER=!TICKER! + 1

		)
	)
)

wget --no-check-certificate  "https://raw.githubusercontent.com/bmrf/tron/master/resources/stage_2_de-bloat/oem/toolbars_BHOs_to_target_by_GUID.txt" -O toolbars.txt

REM Loop through the file...
for /f %%i in (stage_2_de-bloat\oem\toolbars.txt) do (
	REM  ...and for each line: a. check if it is a comment or SET command and b. perform the removal if not
	if not %%i==:: (
	if not %%i==set (
		start /wait msiexec /qn /norestart /x %%i >> "log.txt" 2>nul

		REM Reset UpdateExeVolatile. I guess we could check to see if it's flipped, but eh, not really any point since we're just going to reset it anyway
		reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Updates" /v UpdateExeVolatile /d 0 /f >nul 2>&1

		REM Check if the uninstaller added entries to PendingFileRenameOperations if it did, export the contents, nuke the key value, then continue on
		reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations >nul 2>&1
		if !errorlevel!==0 (
			echo Offending GUID: %%i >> "%COMPUTERNAME%_%CUR_DATE%.txt"
			reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations >> "PendingFileRenameOperations_%COMPUTERNAME%_%CUR_DATE%.txt"
			reg delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations /f >nul 2>&1
			)

		REM Running tick counter to a separate raw log file so we can see if the script stalls on a particular GUID.
		REM Not displayed to console or dumped to main log to avoid cluttering them up
		set /a TICKER=!TICKER! + 1

		)
	)
)

wget --no-check-certificate  "https://raw.githubusercontent.com/bmrf/tron/master/resources/stage_2_de-bloat/oem/programs_to_target_by_name.txt" -O wildcards.txt


REM Loop through the file...
for /f %%i in (wildcards.txt) do (
	REM  ...and for each line: a. check if it is a comment or SET command and b. perform the removal if not
	if not %%i==:: (
	if not %%i==set (
		<NUL "%WMIC%" product where "name like '%%i'" uninstall /nointeractive>> "%LOGPATH%\%LOGFILE%"
		REM Check if the uninstaller added entries to PendingFileRenameOperations if it did, export the contents, nuke the key value, then continue on
		reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations >nul 2>&1
		if !errorlevel!==0 (
			echo Offending entry: %%i >> "%COMPUTERNAME%_%CUR_DATE%.txt"
			reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations >> "%RAW_LOGS%\PendingFileRenameOperations_%COMPUTERNAME%_%CUR_DATE%.txt"
			reg delete "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager" /v PendingFileRenameOperations /f >nul 2>&1
			)
		)
	)
)


ENDLOCAL DISABLEDELAYEDEXPANSION

timeout /t 30

